const snakeCase = require('lodash/snakeCase');
const camelCase = require('lodash/camelCase');
const merge = require('lodash/merge');
const set = require('lodash/set');

const isArray = array => Array.isArray(array);
const isObject = value => typeof value === 'object' && !isArray(value);

function snakeCaseObject(obj) {
  if (!isObject(obj)) return obj;

  return Object.keys(obj).reduce(
    (accum, key) => merge(accum, { [snakeCase(key)]: obj[key] }),
    {},
  );
}

function camelCaseObject(obj) {
  if (!isObject(obj)) return obj;

  return Object.keys(obj).reduce(
    (accum, key) => merge(accum, { [camelCase(key)]: obj[key] }),
    {},
  );
}

function snakeCaseArrayOfObjects(array) {
  if (!isArray(array)) return array;

  return array.map(snakeCaseObject);
}

function camelCaseArrayOfObjects(array) {
  if (!isArray(array)) return array;

  return array.map(camelCaseObject);
}

function snakeCaseObjectDeep(obj) {
  if (!isObject(obj)) return obj;

  const keys = Object.keys(obj);

  const cleanObj = keys.reduce((accum, key) => {
    let value = obj[key];

    if (isArray(value)) value = value.map(snakeCaseObjectDeep);
    if (isObject(value)) value = snakeCaseObjectDeep(value);

    set(accum, snakeCase(key), value);

    return accum;
  }, {});

  return cleanObj;
}

function camelCaseObjectDeep(obj) {
  if (!isObject(obj)) return obj;

  const keys = Object.keys(obj);

  const cleanObj = keys.reduce((accum, key) => {
    let value = obj[key];

    if (isArray(value)) value = value.map(camelCaseObjectDeep);
    if (isObject(value)) value = camelCaseObjectDeep(value);

    set(accum, camelCase(key), value);

    return accum;
  }, {});

  return cleanObj;
}

module.exports.isArray = isArray;
module.exports.isObject = isObject;
module.exports.snakeCaseObject = snakeCaseObject;
module.exports.snakeCaseObjectDeep = snakeCaseObjectDeep;
module.exports.camelCaseObjectDeep = camelCaseObjectDeep;
module.exports.snakeCaseArrayOfObjects = snakeCaseArrayOfObjects;
module.exports.camelCaseArrayOfObjects = camelCaseArrayOfObjects;
